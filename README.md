# New Ventura R

A New Ventura R project remake from android studio migrate to flutter, this is just another complicated project
for more question about this project you can ask to:

* For Gitlab Profile: [Johan Andreas Pranoto](https://gitlab.com/johanandreas74)
* For Gmail: johanandreas74@gmail.com (Johan Andreas Pranoto)

## Requirement
* Flutter version 2.2.3
* Dart 2.13.14
* Android SDK Version 31.0.0

For how to install flutter with android studio or visual studio code, you can go through here first:
```
https://flutter.dev/docs/get-started/editor
```

## Getting Started

Flutter contains the minimal implementation required to create a new library or project.

## How to Use

**Step 1:**

Download or clone this [repo](https://gitlab.com/johanandreas/new-ventura-r).

**Step 2:**

Go to project root and execute the following command in console to get the required dependencies:

```
flutter pub get
```

**Step 3:**
and don't forget to command console this to make sure nothing is wrong with your flutter

```
flutter doctor
```


Here is the folder structure we have been using in this project

```
lib/
|- Api/
|- Modul/
|- main.dart
```
